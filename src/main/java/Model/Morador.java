package Model;

import java.util.ArrayList;

public class Morador {

    private String nome;
    private String apelido;
    private String telefone;
    private String redeSociais;
    private String telefoneResponsável1;
    private String telefoneResponsável2;
    private String republica;
    private ArrayList<Republica> historico;
    private boolean perfilPrivado;

    public Morador(String nome, String apelido, String telefone, String redeSociais, String telefoneResponsável1, String telefoneResponsável2, String republica, boolean perfilPrivado) {
        this.nome = nome;
        this.apelido = apelido;
        this.telefone = telefone;
        this.redeSociais = redeSociais;
        this.telefoneResponsável1 = telefoneResponsável1;
        this.telefoneResponsável2 = telefoneResponsável2;
        this.republica = republica;
        this.perfilPrivado = perfilPrivado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getRedeSociais() {
        return redeSociais;
    }

    public void setRedeSociais(String redeSociais) {
        this.redeSociais = redeSociais;
    }

    public String getTelefoneResponsável1() {
        return telefoneResponsável1;
    }

    public void setTelefoneResponsável1(String telefoneResponsável1) {
        this.telefoneResponsável1 = telefoneResponsável1;
    }

    public String getTelefoneResponsável2() {
        return telefoneResponsável2;
    }

    public void setTelefoneResponsável2(String telefoneResponsável2) {
        this.telefoneResponsável2 = telefoneResponsável2;
    }

    public String getRepublica() {
        return republica;
    }

    public void setRepublica(String republica) {
        this.republica = republica;
    }

    public ArrayList<Republica> getHistorico() {
        return historico;
    }

    public void setHistorico(ArrayList<Republica> historico) {
        this.historico = historico;
    }

    public boolean isPerfilPrivado() {
        return perfilPrivado;
    }

    public void setPerfilPrivado(boolean perfilPrivado) {
        this.perfilPrivado = perfilPrivado;
    }

}
