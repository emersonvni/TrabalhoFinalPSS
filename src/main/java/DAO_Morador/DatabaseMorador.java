package DAO_Morador;


import Model.Morador;


public interface DatabaseMorador {

    public boolean adicionarMorador(Morador morador);

    public boolean removerMorador(Morador morador);

    public Morador getMorador(String nomeMorador);
    
    public void imprimeTudo();

}
