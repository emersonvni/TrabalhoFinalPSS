package DAO_Morador;

import Model.Morador;

import java.util.ArrayList;

public class SQLiteMorador implements DatabaseMorador {

    private static SQLiteMorador _unicaInstancia;
    private ArrayList<Morador> moradores = new ArrayList<Morador>();

    public static SQLiteMorador getInstance() {
        if (_unicaInstancia == null) {
            _unicaInstancia = new SQLiteMorador();
        }
        return _unicaInstancia;
    }

    public SQLiteMorador() {
    }

    @Override
    public boolean adicionarMorador(Morador morador) {
        if (this.moradores.add(morador)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean removerMorador(Morador morador) {
        if (this.moradores.remove(morador)) {
            return true;
        }
        return false;
    }

    @Override
    public Morador getMorador(String nomeMorador) {
        Morador a = null;
        for (Morador morador : moradores) {
            if (morador.getNome().compareTo(nomeMorador) == 0) {
                a = morador;
                break;
            }
        }
        return a;
    }

    public void imprimeTudo() {
        Morador a = null;
        for (Morador morador : moradores) {
            System.out.println(morador.getNome());
        }

    }

}
