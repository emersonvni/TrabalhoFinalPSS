package DAO_Republica;

import Model.Republica;

public interface DatabaseRepublica {

    public boolean adicionarRepublica(Republica republica);

    public boolean removerRepublica(Republica republica);

    public Republica getRepublica(String nomeRepublica);
    
    public void imprimeTudo();

}
