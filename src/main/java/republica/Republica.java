package republica;

import DAO_Republica.SQLiteRepublica;
import presenter.PresenterTelaPrincipal;
import DAO_Republica.DatabaseRepublica;
import conexao.Conexao;
import presenter.Login.LoginPresenter;

public class Republica {

    public static void main(String[] args) {
        
        Conexao conexao = new Conexao();
        conexao.conectar();
        conexao.desconectar();

        new LoginPresenter();
        new PresenterTelaPrincipal().inicial();

    }

}
