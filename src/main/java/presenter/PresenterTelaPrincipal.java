package presenter;

import presenter.ConfirmarSolucao.PresenterP0601;
import presenter.ConsultarResultadoMensalRepublica.PresenterP1101;
import presenter.ConvidarAceitar.PresenterP0501;
import presenter.ConvidarAceitar.PresenterP0502;
import presenter.ManterMoradores.PresenterP0201;
import presenter.ManterPerfil.PresenterP0701;
import presenter.ManterReceitaDespesa.PresenterP0401;
import presenter.ManterReceitaDespesa.PresenterP0402;
import presenter.ManterReceitaDespesa.PresenterP0404;
import presenter.ManterReceitaDespesa.PresenterP0405;
import presenter.ManterReclamacoesSugestoes.PresenterP0801;
import presenter.ManterRepublica.PresenterP0101;
import presenter.ManterRepublica.PresenterP0102;
import presenter.ManterTarefa.PresenterP0301;
import presenter.ManterTarefa.PresenterP0303;
import presenter.histMorador.PresenterP1301;
import view.TelaInicial;

public class PresenterTelaPrincipal {

    public void inicial() {
        TelaInicial telaInicial = new TelaInicial();
        telaInicial.setVisible(true);

        telaInicial.getjMenuItem1().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0102 p = new PresenterP0102();
                p.inicial();
                //P0502 a = new P0502();
            }
        });

        telaInicial.getjMenuItem2().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0201 p = new PresenterP0201();
                p.inicial();
                //P0502 a = new P0502();
            }
        });

        telaInicial.getjMenuItem3().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0601 p = new PresenterP0601();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem4().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0501 p = new PresenterP0501();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem5().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0101 p = new PresenterP0101();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem6().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP1301 p = new PresenterP1301();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem7().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP1101 p = new PresenterP1101();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem8().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0801 p = new PresenterP0801();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem9().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0701 p = new PresenterP0701();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem10().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0502 p = new PresenterP0502();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem11().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0301 p = new PresenterP0301();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem12().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0303 p = new PresenterP0303();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem13().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0401 p = new PresenterP0401();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem14().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0404 p = new PresenterP0404();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem15().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0405 p = new PresenterP0405();
                p.inicial();

            }
        });

        telaInicial.getjMenuItem16().addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //telaInicial.setVisible(false);
                // telaInicial.dispose();

                PresenterP0402 p = new PresenterP0402();
                p.inicial();

            }
        });
    }
}
