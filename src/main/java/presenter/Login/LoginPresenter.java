
package presenter.Login;

import com.pss.senha.validacao.ValidadorSenha;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import view.Login.LoginView;


    public class LoginPresenter {

    private LoginView view;
    private ValidadorSenha validador;

    public LoginPresenter() {
        view = new LoginView();
        validador = new ValidadorSenha();
        

        this.view.getBotaosalvar().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    salvarUsuario();
                } catch (Exception ex) {
                    Logger.getLogger(LoginPresenter.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(view,
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        this.view.setVisible(true);
    }

    private void salvarUsuario() throws Exception {
        StringBuilder sb = new StringBuilder();
        String senha = view.getCamposenha().getText();
        ArrayList<String> resultado = (ArrayList<String>) validador.validar(senha);
        for(String s:resultado){
            sb.append(s).append("\n");
        }
        if (resultado.size()> 0){
            JOptionPane.showMessageDialog(view,sb.toString(),"Senha invalida",
                            JOptionPane.INFORMATION_MESSAGE);
            System.out.println("Nao passou");
        }else 
            System.out.println("passou");
      }
}
